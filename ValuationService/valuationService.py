"""
Valuation service.
Requirements:
fields = ('matching_id', 'total_price', 'avg_price', 'currency', 'ignored_products_count')
"""
import csv
from collections import defaultdict


CURRENCIES = 'data/currencies.csv'
DATA = 'data/data.csv'
MATCHINGS = 'data/matchings.csv'
MAIN_CURRENCY = 'PLN'
END_FILE = 'top_products.csv'


def collect_data(data_file, key="matching_id"):
    """
    Collect data from given file, mapped with key.

    :param data_file: Path to *.csv file with data.
    :param key: String. Default value: 'matching_id'
    :return: Dict. Mapped data.
    """
    collected_data = defaultdict(list)
    for data in csv.DictReader(open(data_file)):
        collected_data[data[key]].append(data)

    return collected_data


def convert_to_pln(price, currency, data_file):
    """
    Convert given currency to PLN value.

    :param price: int or string.
    :param currency: string.
    :param data_file: path to *.csv file with data and ratios
    :return: float. Converted PLN value.
    """
    filtered_currency = filter(
        lambda setup: setup['currency'] == str(currency),
        csv.DictReader(open(data_file))
    )
    if not filtered_currency:
        raise ValueError(
            'Could not find "{currency}" from given "{path}"'.format(
                currency=currency, path=data_file)
        )
    filtered_currency = filtered_currency[0]

    return float(price) * float(filtered_currency['ratio'])


def count_elements(matching_id, prices, data_file):
    """
    divide prices into 2 lists: top counts according to the data_file and rest as an unused.

    :param matching_id: string.
    :param prices: list of prices.
    :param data_file: file with data for top price counts
    :return: tuple. tuple cointains 2 lists.
    """
    limit = filter(
        lambda setup: setup['matching_id'] == str(matching_id),
        csv.DictReader(open(data_file))
    )
    if not limit:
        raise ValueError(
            'Could not filter limit from given {path} and matching_id:{m_id}'.format(
                path=data_file, m_id=matching_id
            )
        )
    limit = limit[0]
    limit = int(limit['top_priced_count'])
    sorted_prices = sorted(prices, reverse=True)

    return sorted_prices[:limit], sorted_prices[limit:]


def average(prices):
    """
    Calculate average for given list.

    :param prices: list of prices.
    :return: float
    """
    try:
        avg = float(sum(prices)) / len(prices)
    except ZeroDivisionError:
        avg = 0.0

    return avg


def collect_results(collected_data, currency):
    """
    Collect results with given collected data and currency

    :param collected_data: mapped dict with clean data
    :param currency: string. static value, for results
    :return: List. List contains dicts as results.
    """
    results = list()
    for m_id, elements in collected_data.iteritems():
        prices = list()
        for e in elements:
            prices.append(
                convert_to_pln(e['price'], e['currency'], CURRENCIES) * int(e['quantity'])
            )
        total_price = max(prices)
        top_prices, ignored_prices = count_elements(m_id, prices, MATCHINGS)

        results.append(
            dict(
                matching_id=m_id,
                total_price=total_price,
                avg_price=average(top_prices),
                currency=currency,
                ignored_products_count=len(ignored_prices)
            )
        )
    return results


def save_results(file_path, results):
    """
    Save results into csv file.

    :param results: List of dicts as results.
    :return: None
    """
    fields = results[0].keys()
    if not fields:
        raise ValueError('Fields are empty. Could not get fields from results.')
    with open(file_path, 'w') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=fields)
        writer.writeheader()
        writer.writerows(results)


def main():
    """ main function """
    cdata = collect_data(DATA)
    results = collect_results(cdata, MAIN_CURRENCY)
    save_results(END_FILE, results)


if __name__ == '__main__':
    main()
