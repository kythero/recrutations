"""
Unit tests for valuation service
"""
import unittest

from valuationService import CURRENCIES, DATA, MATCHINGS, MAIN_CURRENCY
from valuationService import collect_data, convert_to_pln, count_elements, average, save_results, collect_results


class TestValuationService(unittest.TestCase):

    def test_collect_data(self):
        cdata = collect_data(DATA)
        self.assertIsInstance(cdata, dict)
        for key, value in cdata.iteritems():
            self.assertIsInstance(value, list)

    def test_collect_data_nofile(self):
        self.assertRaises(IOError, collect_data, data_file='wrong_file')

    def test_convert_to_pln(self):
        convert = convert_to_pln('300', 'GBP', CURRENCIES)
        self.assertGreater(convert, 600)

    def test_convert_to_pln_nofile(self):
        self.assertRaises(IOError, convert_to_pln, price='300', currency='PLN',
                          data_file='wrong_file')

    def test_convert_to_pln_wrong_currency(self):
        self.assertRaises(ValueError, convert_to_pln, price='300', currency='Unknown',
                          data_file=CURRENCIES)

    def test_count_elements(self):
        use, nouse = count_elements(matching_id=3, prices=[4, 5, 6, 7], data_file=MATCHINGS)
        self.assertIsInstance(use, list)
        self.assertIsInstance(nouse, list)

    def test_count_elements_nofile(self):
        self.assertRaises(IOError, count_elements, matching_id=3, prices=[3, 4], data_file='wrong_file')

    def test_count_elements_wrong_m_id(self):
        self.assertRaises(ValueError, count_elements, matching_id=6, prices=[3, 4],
                          data_file=MATCHINGS)

    def test_average(self):
        avg = average([4, 4])
        self.assertIsInstance(avg, float)
        self.assertEqual(avg, 4.0)

    def test_average_divide_by_zero(self):
        avg = average([])
        self.assertIsInstance(avg, float)
        self.assertEqual(avg, 0.0)

    def test_collect_results(self):
        results = collect_results(collect_data(DATA), MAIN_CURRENCY)
        self.assertIsInstance(results, list)
        for result in results:
            self.assertIsInstance(result, dict)

    def test_save_results_results_empty_or_emptydict(self):
        self.assertRaises(IndexError, save_results, file_path='products.csv', results=[])
        self.assertRaises(ValueError, save_results, file_path='products.csv', results=[dict()])


if __name__ == '__main__':
    unittest.main()
