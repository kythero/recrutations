"""
Fizz Buzz Test.
Program takes 2 inputs(numbers) from a user: from where FizzBuzzTest should 'start' and when 'end'.
Input numbers need to satisfy condition 1 <= start < end <= 10000.
Inputs are given in two lines in console.
"""


def validate_inputs(start, end):
    """
    Validate inputs from user. Condition:
    - both variables have to be integers,
    - start number should be less than end number,
    - start number should be greater than or equal to 1,
    - end number should be less than or equal to 10000

    :param start: int.
    :param end: int.
    :return: tuple. Validated 'start' and 'end' variables.
    """
    if not(isinstance(start, int) and isinstance(end, int)):
        raise TypeError("Integers are required for 'start' and 'end' values.")
    elif start < 1:
        raise ValueError("Your 'start' input cannot be less than 1.")
    elif end > 10000:
        raise ValueError("Your 'end' input cannot be greater than 10'000")
    elif start >= end:
        raise ValueError("Your 'start' input have to be less than 'end'. Make correction :)")

    return start, end+1


def play_fizz_buzz(*args):
    """
    Print results in console for our game.
    if we add single argument, for example '5', our game will printout results from 0 to 4.
    if we add two arguments, for example '5', and '9', our game will print results from 5 to 8.

    :param args: argument for our inner xrange function.
    :return: None
    """
    options = {
        3: 'Fizz',
        5: 'Buzz'
    }
    for i in xrange(*args):
        result = ''.join((options[number] for number in options if i % number == 0))
        output = result if result else i
        print(output)


def main():
    """
    Main function to take inputs from user, validate and run game.
    """
    start = input('Welcome in our FizzBuzz game!\n'
                  'Let me ask you for two numbers.\n'
                  'First value is "start". From which number your game should start?: ')
    end = input('Great, now it\'s time for value "end": ')

    try:
        start, end = validate_inputs(start, end)
        play_fizz_buzz(start, end)
    except (TypeError, ValueError) as ex:
        print('\n'+ex.message)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print("\nGoodBye.")
    finally:
        print("\nTry again!")
